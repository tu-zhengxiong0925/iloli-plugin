![iloli-plugin](https://socialify.git.ci/T060925ZX/iloli-plugin/image?description=1&font=Raleway&forks=1&issues=1&language=1&name=1&owner=1&pattern=Circuit%20Board&pulls=1&stargazers=1&theme=Auto)

<img decoding="async" align=right src="resources/logo.webp" width="35%">

# iloli-plugin

- 一个适用于 [Yunzai 系列机器人框架](https://github.com/yhArcadia/Yunzai-Bot-plugins-index) 的娱乐插件

- 主要是写着玩，菜勿喷

- **使用中遇到问题请加 QQ 群咨询：[1014823769](https://qm.qq.com/q/tkbstruiGI)**

## 安装插件

#### 1. 克隆仓库

    git clone https://github.com/T060925ZX/iloli-plugin.git ./plugins/iloli-plugin

> [!NOTE]
> 如果你的网络环境较差，无法连接到 Github，可以使用 [GitHub Proxy](https://ghproxy.link/) 提供的文件代理加速下载服务
>
> ```
> git clone https://ghgo.xyz/https://github.com/T060925ZX/iloli-plugin.git ./plugins/iloli-plugin
> ```
> Gitee源（更新较慢）：
> ```
> git clone https://gitee.com/tu-zhengxiong0925/iloli-plugin ./plugins/iloli-plugin
> ```

#### 2. 安装依赖

```
pnpm install --filter=iloli-plugin
```

## 功能列表

请使用 `#i帮助` 获取完整帮助

- [x] 自动更新


<div align="left"> 
  
![Visitor Count](https://profile-counter.glitch.me/T060925ZX/count.svg)
  
</div>

## 支持与贡献

如果你喜欢这个项目，请不妨点个 Star🌟，这是对开发者最大的动力 呜咪~❤️

有意见或者建议也欢迎提交 [Issues](https://github.com/T060925ZX/iloli-plugin/issues) 和 [Pull requests](https://github.com/T060925ZX/iloli-plugin/pulls)

### 感谢那些默默付出的人
###### ***不分先后顺序***

| 名单 | 主要贡献 |
|:----: |:----: |
| [@Jiaozi](https://github.com/T060925ZX) | 新建文件夹（bushi |


> [!TIP]
> Jiaozi插件交流群 [983299064](https://qm.qq.com/q/ciLUvOjDyw) 
